#!/bin/bash

RED='\033[1;31m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
WHITE='\033[1;37m'
YELLOW='\e[33m'
NC='\033[0m'

INFO="${CYAN}[info]${NC}"
WARN="${YELLOW}[warning]${NC}"
FATAL="${RED}[fatal]${NC}"