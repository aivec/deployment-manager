#!/bin/bash

. colors.sh

zip_project() {
    i=$1 # config index

    config=$(cat deployments-config.json | jq -r --arg index "$i" '.[$index | tonumber]')
    project_name=$(echo $config | jq -r '.["project_name"]')
    project_abspath=$(echo $config | jq -r '.["project_abspath"]')
    deployments_folder=$(echo $config | jq -r '.["deployments_folder"]')

    # go to project dir
    pushd $project_abspath
    VERSION=$(git describe | sed 's/v//g')
    PLUGIN_NAME=${PWD##*/}
    PLUGIN_ZIPNAME=$PLUGIN_NAME.$VERSION

    if [ ! -e "bundle.sh" ]; then
        echo -e "\n${WARN} bundle.sh does not exist in project folder. Skipping...\n"
        popd
        return
    fi

    # zip the plugin and source the deployments folder path
    echo -e "\n${WHITE}=========================================================================="
    echo -e "${INFO} ${WHITE}Executing zip script for ${GREEN}${project_name}${WHITE} \u2193\u2193\u2193\u2193\u2193${NC}\n"

    ./bundle.sh
    if [ ! -d $deployments_folder ]; then
        mkdir -p $deployments_folder
    fi
    if [ ! -d $deployments_folder/$PLUGIN_NAME ]; then
        mkdir $deployments_folder/$PLUGIN_NAME
    fi
    if [ -d $deployments_folder/$PLUGIN_NAME/$PLUGIN_NAME ]; then
        rm -r $deployments_folder/$PLUGIN_NAME/$PLUGIN_NAME
    fi
    mv $PLUGIN_ZIPNAME.zip $deployments_folder/$PLUGIN_NAME/$PLUGIN_ZIPNAME.zip
    pushd $deployments_folder/$PLUGIN_NAME
    unzip $PLUGIN_ZIPNAME.zip
    popd
}