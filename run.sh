#!/bin/bash

. colors.sh
. zip.sh
. staging_upload.sh

command -v jq >/dev/null 2>&1 || {
    echo >&2 -e "${FATAL} 'jq' is required to parse the JSON config file. Please install it. https://stedolan.github.io/jq/download"
    exit 1
}

command -v lftp >/dev/null 2>&1 || {
    echo >&2 -e "${FATAL} 'lftp' is required to upload via FTP. Either it isn't installed, or it isn't in your \$PATH."
    exit 1
}

command -v ssh >/dev/null 2>&1 || {
    echo >&2 -e "${FATAL} 'ssh' is required to upload via SCP. Either it isn't installed, or it isn't in your \$PATH."
    exit 1
}

command -v scp >/dev/null 2>&1 || {
    echo >&2 -e "${FATAL} 'scp' is required to upload via SCP. Either it isn't installed, or it isn't in your \$PATH."
    exit 1
}

command -v sshpass >/dev/null 2>&1 || {
    echo >&2 -e "${FATAL} 'sshpass' is required to upload via SCP. Either it isn't installed, or it isn't in your \$PATH."
    exit 1
}

quotes() {
    arr[0]="'I wish it need not have happened in my time,' said Frodo. 'So do I,' said Gandalf, 'and so do all who live to see such times. But that is not for them to decide. All we have to decide is what to do with the time that is given us.'"
    arr[1]="Faithless is he that says farewell when the road darkens."
    arr[2]="It's the job that's never started as takes longest to finish."
    arr[3]="Not all those who wander are lost."
    arr[4]="It's a dangerous business, Frodo, going out your door. You step onto the road, and if you don't keep your feet, there's no knowing where you might be swept off to."
    rand=$(($RANDOM % ${#arr[@]}))

    echo -e "${NC}\n"\""${GREEN}${arr[$rand]}${NC}\""${CYAN}" - J.R.R. Tolkien"${NC}"\n"
}

projectcount=$(cat deployments-config.json | jq -r '. | length')
projectcount=$(($projectcount - 1))
projects=()
declare -A indexmap

i=0
while [ $i -le $projectcount ]; do
    pname=$(cat deployments-config.json | jq -r --arg index "$i" '.[$index | tonumber]["project_name"]')
    indexmap["$pname"]=$i
    projects+=("$pname")
    i=$(($i + 1))
done
if [ $i -gt 1 ]; then
    projects+=("all")
fi

selectedproject=$1
if [ -z ${1+x} ]; then
    PS3='Select a project: '
    select selectedproject in "${projects[@]}"; do
        echo -e "\n"
        break
    done
fi

if [ -z "${indexmap[$selectedproject]}" ]; then
    echo Project \'"$selectedproject"\' does not exist
    exit 1
fi

declare -A actionmap
actionmap["upload"]=upload_to_staging
actionmap["zip"]=zip_project

if [ -z ${2+x} ]; then
    while true; do
        read -p "1) Upload to staging environment
2) Zip Project
3) Deploy to production
Select an action to perform for $selectedproject: " answer
        case $answer in
        [1]*)
            if [ "$selectedproject" == 'all' ]; then
                i=0
                while [ $i -le $projectcount ]; do
                    ${actionmap[upload]} "$i"
                    i=$(($i + 1))
                done
            else
                ${actionmap[upload]} "${indexmap[$selectedproject]}"
            fi
            quotes
            exit
            ;;
        [2]*)
            if [ "$selectedproject" == 'all' ]; then
                i=0
                while [ $i -le $projectcount ]; do
                    ${actionmap[zip]} "$i"
                    i=$(($i + 1))
                done
            else
                ${actionmap[zip]} "${indexmap[$selectedproject]}"
            fi
            quotes
            exit
            ;;
        [3]*)
            echo "This action is not yet implemented :(. Aborting."
            # echo -e "\n${INFO} ${WHITE}Stopping Container(s)...${NC}"
            # if [ "$selectedproject" == 'all' ]; then
            #     i=0
            #     while [ $i -le $projectcount ]; do
            #         stopContainer "$i"
            #         i=$(($i + 1))
            #     done
            #     docker-compose -p ${NETWORK_NAME} -f docker-compose.db.yml down
            # else
            #     stopContainer "${indexmap[$selectedproject]}"
            # fi
            exit
            ;;
        *) echo "Please select an action" ;;
        esac
    done
else
    if [ -z ${actionmap[$2]+abc} ]; then
        echo -e "'$2' is not a valid action. Please pass one of 'upload' or 'zip'"
    else
        ${actionmap[$2]} "${indexmap[$selectedproject]}"
    fi
fi
