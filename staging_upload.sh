#!/bin/bash

. colors.sh
. zip.sh

upload_to_staging() {
    i=$1 # config index
    config=$(cat deployments-config.json | jq -r --arg index "$i" '.[$index | tonumber]')
    zip_project "$1"
    ftp_upload "$config"
    scp_upload "$config"
}

ftp_upload() {
    config=$1

    project_abspath=$(echo $config | jq -r '.["project_abspath"]')
    deployments_folder=$(echo $config | jq -r '.["deployments_folder"]')
    staging_config_ftp=$(echo $config | jq -r '.["staging_config_ftp"]')
    if [ "$staging_config_ftp" == "null" ] || [ -z "$staging_config_ftp" ]; then
        echo -e "\n${INFO} ${WHITE}FTP config not set. Skipping...${NC}\n"
        return
    fi

    destcount=$(echo $staging_config_ftp | jq -r '. | length')
    destcount=$(($destcount - 1))
    index=0
    while [ $index -le $destcount ]; do
        destconfig=$(echo $staging_config_ftp | jq -r --arg index "$index" '.[$index | tonumber]')

        endpoint=$(echo $destconfig | jq -r '.["endpoint"]')
        ftp_host=$(echo $destconfig | jq -r '.["ftp_host"]')
        ftp_user=$(echo $destconfig | jq -r '.["ftp_user"]')
        ftp_password=$(echo $destconfig | jq -r '.["ftp_password"]')
        put_path=$(echo $destconfig | jq -r '.["put_path"]')

        # go to project dir
        pushd $project_abspath
        VERSION=$(git describe | sed 's/v//g')
        PLUGIN_NAME=${PWD##*/}

        echo -e "\n${WHITE}=========================================================================="
        echo -e "${INFO} ${WHITE}Uploading ${GREEN}${PLUGIN_NAME}.${VERSION} ${WHITE}to ${CYAN}${endpoint}${NC}\n"

        pushd $deployments_folder/$PLUGIN_NAME

        echo -e "\n${INFO} ${WHITE}Uploading plugin to test site via lftp. This may take some time...${NC}"
        if [ -z "$ftp_user" ] || [ -z "$ftp_password" ]; then
            lftp $ftp_host <<HERE
cd $put_path
mkdir -p $PLUGIN_NAME
rm -r $PLUGIN_NAME
mirror -R $PLUGIN_NAME $PLUGIN_NAME
ls
quit
HERE
        else
            lftp -u $ftp_user,$ftp_password $ftp_host <<HERE
cd $put_path
mkdir -p $PLUGIN_NAME
rm -r $PLUGIN_NAME
mirror -R $PLUGIN_NAME $PLUGIN_NAME
ls
quit
HERE
        fi

        echo -e "\n${INFO} ${WHITE}Upload complete.\n"
        popd
        index=$(($index + 1))
    done
}

scp_upload() {
    config=$1

    project_abspath=$(echo $config | jq -r '.["project_abspath"]')
    deployments_folder=$(echo $config | jq -r '.["deployments_folder"]')
    staging_config_ssh=$(echo $config | jq -r '.["staging_config_ssh"]')
    if [ "$staging_config_ssh" == "null" ] || [ -z "$staging_config_ssh" ]; then
        echo -e "\n${INFO} ${WHITE}SCP config not set. Skipping...${NC}\n"
        return
    fi

    destcount=$(echo $staging_config_ssh | jq -r '. | length')
    destcount=$(($destcount - 1))
    index=0
    while [ $index -le $destcount ]; do
        destconfig=$(echo $staging_config_ssh | jq -r --arg index "$index" '.[$index | tonumber]')

        host=$(echo $destconfig | jq -r '.["host"]')
        user=$(echo $destconfig | jq -r '.["user"]')
        private_key=$(echo $destconfig | jq -r '.["private_key"]')
        put_path=$(echo $destconfig | jq -r '.["put_path"]')
        password_file=$(echo $destconfig | jq -r '.["password_file"]')
        password_prompt_search_string=$(echo $destconfig | jq -r '.["password_prompt_search_string"]')
        if [ "$password_prompt_search_string" == "null" ] || [ -z "$password_prompt_search_string" ]; then
            password_prompt_search_string=""
        else
            password_prompt_search_string="-P $password_prompt_search_string"
        fi

        # go to project dir
        pushd $project_abspath
        VERSION=$(git describe | sed 's/v//g')
        PLUGIN_NAME=${PWD##*/}

        echo -e "\n${WHITE}=========================================================================="
        echo -e "${INFO} ${WHITE}Uploading ${GREEN}${PLUGIN_NAME}.${VERSION} ${WHITE}to ${CYAN}${host}${NC}\n"

        pushd $deployments_folder/$PLUGIN_NAME
        echo -e "\n${INFO} ${WHITE}Uploading plugin to test site via scp. This may take some time...${NC}"
        if [ "$password_file" == "null" ] || [ -z "$password_file" ]; then
            ssh -i ${private_key} ${user}@${host} "cd ${put_path} && rm -r $PLUGIN_NAME"
            scp -r -i ${private_key} $PLUGIN_NAME ${user}@${host}:./${put_path}/
        else
            sshpass -v ${password_prompt_search_string} -f $password_file ssh -i ${private_key} ${user}@${host} "cd ${put_path} && rm -r $PLUGIN_NAME"
            sshpass -v ${password_prompt_search_string} -f $password_file scp -r -i ${private_key} $PLUGIN_NAME ${user}@${host}:./${put_path}/
        fi

        echo -e "\n${INFO} ${WHITE}Upload complete.\n"
        popd
        index=$(($index + 1))
    done
}
